# Energy Management

## 1. Manage Energy not Time

### Question 1:
#### What are the activities you do that make you relax - Calm quadrant?

Activities that I do to relax myself are:
- Playing chess while hearing music
- Playing games
- Talk to my friends
- Going out for a walk


### Question 2:

#### When do you find getting into the Stress quadrant?

I tend to get inside stress quadrant when:

- Times when my exams are nearing
- Got assigned with a lot of workload
- Got assigned with a task that I have no idea about


### Question 3:
  
#### How do you understand if you are in the Excitement quadrant?
I feel myself in an excitement quadrant when

- my code works without any errors
- achieved a task that I worked on so hard
- riding a Royal Enfield bike

## 4. Sleep is your superpower

### Question 4

#### Paraphrase the Sleep is your Superpower video in detail.

- Sleeplessness can age a man by a decade.
- Our brain requires a 8-hour sleep inorder to perform more efficiently.
- Also sleep is required after learning in order to save our learning.
- Memory power increases with sleep.
- Decrease in sleep among people may lead to heart attacks, car accidents and suicides.
- Natural killer cells work best when our body gets good sleep.
- Gene manipulation can occur when there is a sleep deficiency which may lead to cancer, tumors, chronic diseases, and cardiovascular diseases.
- Sleep is not a luxury, it is a biological necessity


### Question 5

#### What are some ideas that you can implement to sleep better?
- Regularity: Going to bed at the same time and waking up at a same time
- Keeping coolness: The body requies decrease in heat to sleep properly.
- Make rooms as dim as possible while going to sleep
- Avoid media consumption prior going to bed

## 5. Brain Changing Benefits of Exercise
### Question 6

#### Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points.

- Brain is the most complex structure known to humankind
- After every sweat-inducing workout our brain gets a energy boost.

- Workouts will induce good moods, will increase reaction times and make our body active.

- Regular exercises will increase the number of brain cells which will result in increase of volume in the hippocampus.

- Workouts result in long-lasting effects in an energy boost and mental wellness.

- We need to workout anywhere from 3 to 30 minutes  to get started with the exercise
### Question 7

#### What are some steps you can take to exercise more?
- Going to the gym and workout regularly
- Play outdoor games like football, volleyball etc.
- Motivate ourself to workout more keeping its good efects in mind.
- Take small steps like increasing your push-up count one by one a day to your desired limit.