# Basic JavaScript Datatypes and Data Structures with code samples.

In Javascript, variables are not directly associated with any data type.

## Dynamic and weak typing

Javascript automatically assigns the data type of the value assigned to the variable, and hence it is **dynamic**. This also means that the same variable can be assigned with different data types.

Here are some code examples:
``` 
let a = 10; // 10 is now a integer
a = "text"; // a is now a text
a = true; // a is now a boolean 
```
Javascript is also **weakly typed** which means it allows implicit type conversions and won't throw an error even when operations are done with mismatched data types.

Let us see an example code:

```
const a = "test"; // variable 'a' is a string
let b = a + 1; // operation involves string and integer
console.log(b); //output will be test1
```

## Primitive and Non-Primitive data types in JavaScript

### Primitive Data types

Primitive data types are the ones that are predefined by JavaScript language and are available by default to use. 

Primitive datatypes of JavaScript are: 

1. **Numbers**: Numbers in javascript can store values with or without decimals. JavaScript can store floating values in the range of ± (2<sup>-1074</sup> to 2<sup>1024</sup>). <br><br> It can store integers in the range of ±  (2<sup>53</sup>-1). Values beyond the maximum range will go to infinity or zero based on closer boundary limits.

Code Sample:
```
let x = 100;
let y = 2.3;
console.log(x,y); // output: 100 2.3
```
2. **String**: Strings represents a sequence of characters inside double quotes or single quotes. In JavaScript, strings are immutable which means we cannot modify them once it is declared and a value is assigned to them.

Code Sample:
```
let str= "Hello World";
console.log(str); // output: Hello World
```
3. **Undefined**: Undefined refers to a variable whose value is not yet assigned.

Code Sample:
```
let z;
console.log(z); //Output: Undefined
```
4. **Null**: This datatype can hold only one possible value which is null.

Code Sample:
```
let x= null;
console.log(x); //output: null
```
5. **Boolean**: The Boolean data type accepts two possible values and they are 'true' or 'false'.

Code Sample:
```
let bool = true;
console.log(bool , !bool); //output= true false
```
### Non-Primitive Data type
1. **Objects**: In JavaScript, objects are mutable values. In addition, functions are also treated as variables in JavaScript with the capability of being callable.<br><br>Objects can also be seen as collection of properties. Object properties can be related with key-value pairs. Hence property value can be of any type, including other objects which enables users to build more complex data structures. <br><br> There are two types of object properties in JavaScript:
    * Data Property
    * Accessor Property


Defining an object in JavaScript:
* Using Constructor function 
```
var obj = new Object(); // Creates an empty generic object

var tree= new Tree(); //Creates an user defined object
```
* Using Literal notations
```
var bike={}; // Empty object
var car={a:5, b:10}; // user-defined object with 5 and 10 being values of the keys a and b
```

2. **Arrays**: In JavaScript, more than one element of various data types can be stored under a single name.

Code sample:
```
var items = new Array(); //Array with no elements

var elements= new Array(1,2,'Hello'); //Array with two or more elements of various data types.
```
_______
## References:
* [JavaScript data types and data structures](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures)
* [Primitive and Non-primitive data-types in JavaScript](https://www.geeksforgeeks.org/primitive-and-non-primitive-data-types-in-javascript/)
 * [Javascript Data Types](https://www.javatpoint.com/javascript-data-types)