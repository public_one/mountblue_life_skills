# Learning Process
### 1. How to learn faster with the Feynman Technique
#### What is the Feynman Technique? Paraphrase the video in your own words.
* Feynman technique is a concept of breaking down complex concepts into simple english (In layman terms).
* It is a four step process which includes:
    1. Write the name of the concept.
    2. Explain the concept to yourself in plain english.
    3. Identify the areas where you are struggling and revisit those areas
    4. Pinpoint if there are any technical terms involved and further simplify them

#### What are the different ways to implement this technique in your learning process?
* Explain the concept to ourselves in simple English.
* Explain the concept we are trying to learn to a kid.
* Explain it to our friend who has no idea about the concept and see if he can understand that.
### 2. Learning How to Learn TED talk by Barbara Oakley
#### Paraphrase the video in detail in your own words.
* Brain is enormously complex. While we are trying to learn it can get into two modes:
    1. Focus mode: Mode that eables us to keep complete focus on something.
    2. Diffuse mode: Mode where new ideas can emerge.
* We must always allow our brain to move back and forth between two these two modes by transferring new and fresh ideas from diffuse mode to focus mode.
* Procastination is addictive and may lead to problems.
* Pomodoro Technique to avoid procastination:
    * Set a timer for 25 minutes and work with complete focus for those 25 minutes
    * After the focus is over, take some time to relax and start again
* Effectives ways to learn something: Exercise,recall and test
* Recalling what we learnt enhances our neural hooks
#### What are some of the steps that you can take to improve your learning process?
* To use the pomodoro technique to work on something with focus
* Practice and solve problems more than once until solution flows into our mind like a song.
* Recall at certain points to remember what we had learnt so far
### 3. Learn Anything in 20 hours
#### Your key takeaways from the video? Paraphrase your understanding.
* Learning a new skill is a exponential curve. At the time we start, we need to put in much effort and time since the skill we are trying to learn is new. Once we get to a point, learning further usually becomes easy and faster.
* It only takes 20 hours of focused learning in-order to get good at something.
* There are three steps in order to achieve this
    1. Deconstruct the skill
    2. Learning just enough to self correct
    3. Remove practice barriers
#### What are some of the steps that you can while approaching a new topic?
* To breakdown the topic into sections and learn the most important ones first.
* Practice at points where we had learnt enough to put our skills into action.
* Pre-commit ourselves to the skill or topic we are trying to lear and overcome the initial frustation period.