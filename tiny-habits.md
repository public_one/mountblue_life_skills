# Tiny Habits

### Question 1

#### Your key take-aways from video
- We must develop a new tiny habit which should be done over a existing habit.
- These tiny habits should be something that gives us energy.
- To do a habit we must have adequate amount of will power and interest.
- If the task is too easy or too hard, we usually won't have motivation to do those tasks.
- If a task is too hard, we must break them into small parts and do them.

### Question 2

#### Your takeaways from the video in as much detail as possible
- Formula for human behaviour is B=MAP i.e Motivation, Ability and Prompt converge at the same time.
- Shrink the behaviour: Think of a behaviour to its smallest version.
- Identify an Action Prompt: 
    1. External Prompt - cues from environment
    2. Internal Prompt - thoughts and sensations
    3. Action Prompt -completion of one action reminds you of another

### Question 3

#### How can you use B = MAP to make making new habits easier?
 If we are already doing a habit make use of that particular habit (behaviour) to develop a another tiny habit since the behaviour already has motivaation and moment which could get carried on to the next tiny habit.

### Question 4

#### Why it is important to "Shine" or Celebrate after each successful completion of habit?
It is important to celebrate each successful completion of task since it boosts our motivation and happiness.
### Question 5

#### Your takeaways from the video (Minimum 5 points)
- We need to put more steps between us and the bad habits and fewer steps between us and good habits.
- If we are about to do a task or project, fast forward our thoughts to future and imagine that our plan failed then figure out what were the things that made our plan fail and rectify them so that our plan doesn't fail for real.
- Do not crave for things that gives short term happiness but has consequences over a period of time. We should stick to habits that gives long term benefits
- Get back on track more quicky even if the chain breaks or streak you wanted to maintain didn't go so well
- Tiny habits makes the path to someone what we desire to become
### Question 6

#### Write about the book's perspective on habit formation from the lens of Identity, processes and outcomes?
Instead of focusing on outcome of a habit, one should focus on the identity and process behind it. He also argues that small, increamental changes to a habit can significatly make improvements over long-term.

### Question 7

#### Write about the book's perspective on how to make a good habit easier?
Try to reduce the steps between you and good habits. Make them easy to access so that you can achieve it easily.
### Question 8

#### Write about the book's perspective on making a bad habit more difficult?
Try to add more steps between you and bad hit so that you can't access the bad habit easily.

### Question 9:

#### Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?

I would like to make a habit of playing guitar. There are the steps I could take to play guitar more easier:

- Able to play a song of my favourite hero.
- Able to showcase my music skills to my family.
- Celebrating after each phase of learning guitar.

### Question 10:

#### Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?

The habit I would like to eliminate is looking at social media immediately after waking up. These are the steps I could take to eliminate that habit:

- Go out for a walk and have a tea that would be satisfying 
- Take a hot shower and take my breakfast. So that I won't waste any time on social media immediately after waking up.
- Hide social media apps to a folder and making it diffuclt to access.