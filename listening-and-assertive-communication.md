# Listening and Active Communication

## 1. Active Listening

### What are the steps/strategies to do Active Listening?
* Avoid drowning in your own thoughts. Focus on the speaker and listen to him/her.

* Do not interrupt the speaker while he is sharing the information instead wait for the person to finish his/her speech and then respond.

* Use door openers and give compliments while another person is speaking so that it makes the speaker engaged and he/she will share more information.

* Use body language to express that you are actively listening.

* Take notes during important conversations.

* Paraphrase what you have listened to so far and get verified with the speaker to make sure you both are on the same page.


## 2. Reflective Listening 

### According to Fisher's model, what are the key points of Reflective Listening? 
* To pay attention to the speaker and try to understand and feel the speaker's idea. After listening to the idea, we reflect our understanding back to the speaker and get it verified.

* Empathy: Try to feel what the speaker is conveying in his speech.

* Provide feedback to the speaker using body language and compliments.

* Ask questions in case you are in doubt to make sure you are on the same page with the speaker. This can display your interest in the information the speaker is delivering.


## 3. Reflection

### What are the obstacles in your listening process?
Obstacles in my listening process can include:
 * Drowning in my own thoughts while the speaker is delivering his speech.

 * If the speaker shares his idea or his feelings. I'll get a grasp of part of his idea and think about that idea even though the speaker has not fully completed his speech.

### What can you do to improve your listening?
To improve my listening I could use techniques mentioned in active listening such as:
 * Avoid drowning in my own thoughts and instead focus on the speaker and listen to him


## 4. Types of Communication

### When do you switch to a Passive communication style in your day-to-day life?

I would communicate passively with elders and persons of authority. Since their needs come before us and we can't speak up against them just like we do with others.

### When do you switch into Aggressive communication styles in your day to day life?
I would generally not get into any form of aggressive communication style. In some rare scenarios, I would act aggressively if some person is trying to cheat or scam me.

### When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day-to-day life?
I would switch to passive Aggressive style when I am with my friends and close family members. Since they won't get offended and will usually reply with the same communication style I used on them.
### How can you make your communication assertive? You can analyze the videos and then think about what steps you can apply in your own life. 
Assertive communication is generally a learned skill and usually does not come naturally. There are certain steps to make our communication assertive.
* Try being assertive with your close friends and family members because it is a baby step to learning to be assertive. Our close friends would generally agree and understand our emotions.
* Try to name an emotion/feeling. Telling people how you feel can make them stop if they are being rude or mean.
* Recognize my needs and share them with other people so that we have a mutual understanding of how things should go on.
* Be aware of my state and body language.
* Call out behaviors that hurt me.