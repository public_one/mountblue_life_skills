# Focus Management

## 1. What is Deep Work

### Question 1
#### What is Deep Work?
Deep work is focusing without distraction on a cognitively demanding task

- To do deep work we need focus as the primary tier one skill.
- Deep work's duration can be from 1 hour to 4 hour
- During this hour our attention must be only on one core concept.

## 4. Summary of Deep Work Book

### Question 2
#### Paraphrase all the ideas in the above videos and this one in detail.

1. Schedule distractions: We must not be distracted to every single mobile notification or email notification. Instead we must allocate a period of time to check phone and notification. Since those will afffect our focus.

2. Deep work ritual: We must atleast work deeply one hour per day inorder to be productive.

3. Evening shutdown is making a list of pending tasks which needs to be done thext day and getting enough sleep. Sleepi s the price we pay for focus.


### Question 3
#### How can you implement the principles in your day to day life?
- Put my mobile in Focus mode where incoming notifications will be limited.
- Focusing on the task without any distraction and implement deepwork concept
- Evening shutdown: Resting enough inorder to acquire the enery needed for next day's work

## 5. Dangers of Social Media

### Question 4
#### Key takeaways from the video

- High usage of social media could decrease our poductivity.
- Social media is designed to be addictive and hence usage of it must be reduced
- We have created a fake scenario that people will forget us if don't use social media.
- Focus on creating something that is hard to reproduce by someone