# Prevention of Sexual Harassment
### What kinds of behaviour cause sexual harassment?
 Three forms of sexual harassment include:
1. Verbal:
    * Inappropriate comments about a person's clothing or body.
    * Making sexual or gender-based remarks.
    * Requesting sexual favors from a person
    * Sexual Threats that make employees humiliated, uncomfortable or intimidated
    * Spreading Rumours about a person's personal or sexual life
    * Foul and obscene language
2. Visual:
It is a type of harassment where the victim feels uncomfortable when seeing inappropriate content which may include:
    * Obscene posters
    * Drawings
    * Pictures
    * Cartoons
    * Emails or Texts
    * Hand Gestures
    
3. Physical:
    * Sexual Assault
    * Impeding or blocking movement
    * Inappropriate touching
    * Sexual gestures, leering, and staring

Other two categories of sexual harassment include:
1. Hostile work environment - It is a type of environment where actions of co-workers, supervisors, or managers severely impact the ability of the victim to perform his job and make him feel uncomfortable.

2. Quid Pro Quo - This type of harassment is usually perpetrated by someone who is in a position of power or authority over another individual.

### What would you do in case you face or witness any incident or repeated incidents of such behavior?
Sexual harassment is a work place hazard that may put our health and safety to risk. So, employees must immediately report such incidents to the harassment prevention committee. 

In case I witness any such incident or behavior, I would immediately report it to the concerned committee.