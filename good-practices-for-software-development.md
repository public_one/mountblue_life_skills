# Good Practices for Software Development

### Question 1
#### What is your one major takeaway from each one of the 6 sections. So 6 points in total.
1. Asking questions and seeking clarity in meeting itself. Since most of the times, it will be difficult to get the same set of people online again

2. Use the group chat/channels to communicate most of the time. This is preferable over private DMs simply because there are more eyes on the main channel.

3. Explain the problem clearly, mention the solutions you tried out to fix the problem

4. Make time for your company, the product you are working on, and your team members. This will help a lot in improving your communication with the team.

5. Be available when someone replies to your message. Things move faster if it's a real-time conversation as compared to a back and forth every hour.

6. Work when you work, play when you play is a good thumb rule.

### Question 2

#### Which area do you think you need to improve on? What are your ideas to make progress in that area?

Areas I think that I need to improve on are:

* Communcation with team members since I generally a bit hesitant to communicate. So, I am constantly pushing myself to communicate and seek clarity over anything.

* Susctained concentration: I tend to deviate when an external prompt crosses my way and after which it takes me some time to get concentration on that particular tasks. So I am trying to decrease the number of external prompts inorder to be fullly concetrated.