# Grit and Growth Mindset

### 1. Grit
#### Paraphrase (summarize) the video in a few lines. Use your own words.
- Speaker was a 7th-grade teacher and she found that IQ was not the only difference between her worst and best students.

- Students will the highest IQ did not perform that well at school and those who performed well did not have the strongest IQ

- It had kept her thinking and she realized that doing well in life has much more to do than learning something much more quickly in school

- Through her research in partnership with a private company she found that to be successful there was one key factor called grit.

- Grit is passion and perseverance toward long-term commitments.

- Grit is staying with your passion for years and working on it with your stamina. Grit is working for the future you dream of to make it a reality

- Talent doesn't make someone gritty. Grit is inversely related to measures of talent

- To develop grit we need to have a growth mindset. A growth mindset is one's ability to learn is not fixed. It tends to grow with our effort.

- Our brain doesn't believe that failure is a permanent condition and it tends to grow to face challenges.

#### What are your key takeaways from the video to take action on?

- To develop grit and have perseverance for my long-term commitments.

- To have a growth mindset and not take failures as a permanent condition. Rather use my failures to grow and learn.

- Dreams can become a reality when we stick on to them for years with passion and perseverance.

### 2. Introduction to Growth Mindset

#### Paraphrase (summarize) the video in a few lines in your own words.
- There are two types of mindset and they are fixed mindset and growth mindset
- People with a fixed mindset believe that skills are born and whereas people with a growth mindset believe that skills are made and developed.
- People with a growth mindset tend to believe that our ability to do something is directly proportional to the efforts and time we put in.
- Growth mindset is the foundation for great learning
- People with a growth mindset focus on trying to do better whereas people with a fixed mindset focus on performance and not looking bad at the outcome.
- People with a growth mindset believe that Efforts lead to growth, challenges frame an opportunity, and use failures to learn and appreciate the feedback.

#### What are your key takeaways from the video to take action on?
- Skills are made, not born. We need to put in time and effort to improve our ability to do something

- Focus on getting better rather than focusing on outcomes

- Growth mindset is the foundation for great learning

### 3. Understanding Internal Locus of Control

#### What is the Internal Locus of Control? What is the key point in the video?

- Internal locus of control is believing that desired results are achieved by the degree of effort we put in or by actions we have control over.

- Key point in this video is to believe that we are in control over our destiny.

### 4. How to build a Growth Mindset

#### Paraphrase (summarize) the video in a few lines in your own words.
- Always believe in our ability to figure things out.
- Without belief, there is no ability to grow as well as expectency or agency.
- Question our assumptions of who you are ad think of what you can become.
- Do not box down your ability with your current knowledge and skills. Instead, focus on the vision to improve.
- Think about the path you have come by and what kind of person you have become over the days.
- Develop our own life Curriculum
- Honour the struggles that come our way.
#### What are your key takeaways from the video to take action on?
- Honour the struggles because they are the ones that make us a better person.
- Be always open to learn.
- Believe in our ability to figure things out.

### 5. Mindset - A MountBlue Warrior Reference Manual
#### What are one or more points that you want to take action on from the manual? 

- I will treat new concepts and situations as learning opportunities. I will never get pressurized by any concept or situation.

- I will stay with a problem till I complete it. I will not quit the problem.

- I will always be enthusiastic. I will have a smile on my face when I face new challenges or meet people.